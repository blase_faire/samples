var expect    = require("chai").expect;
var allocator = require("../app/allocations");
var fs		  = require('fs');

describe("Allocation Calculator", function() {
  describe("Calculate Total Allocation", function() {
   	
	it("Ensures we have some basic validation: roles and names must exist", function() {
    	var blankBudget = allocator.calculateBudget([ { "role": "Manager" } ]);

    	expect(blankBudget).to.equal("ERROR IN INPUT");
    });

    it("Returns a single total budget of zero given an empty hierarchy of roles", function() {
    	var blankBudget = allocator.calculateBudget([]);

    	expect(blankBudget.total).to.equal(0);
    });

    it("Total the allocation given a single role", function() {
    	var managerBudget = allocator.calculateBudget([ { "role": "Manager", "name": "Bob" } ]);

    	expect(managerBudget.total).to.equal(300);
    });

    it("Calculate total budget given a flat list of roles", function() {
    	var managerBudget = allocator.calculateBudget(
    		[ { "role": "Manager", "name": "Podunk" }, 
    			{ "role": "Manager", "name": "Tim" }, 
    			{ "role": "QA Tester", "name": "James" } ]);

    	expect(managerBudget.total).to.equal(1100);
    });

	// Confirm that we can look up role budget ammounts individually
	it("Look up the roles we know about, verify the amounts", function() {
		var managerAmount = allocator.lookupBudgetForRole("Manager");
		var testerAmount = allocator.lookupBudgetForRole("QA Tester");
		var developerAmount = allocator.lookupBudgetForRole("Developer");

		expect(managerAmount).to.equal(300);
		expect(testerAmount).to.equal(500);
		expect(developerAmount).to.equal(1000);
	});

	// First, let's enrich the data so that it's easy to determine individual role allocations
	it("Creates a new map with allocation data in it. Verify new properties exist", function() {
		var hierarchy = JSON.parse( fs.readFileSync('test/data_5400.json', 'utf-8') );
    	var enrichedMap = allocator.createMapWithAllocations(hierarchy);

    	enrichedMap.forEach(function(item) {
    		expect(item).to.have.property('allocation');
    		expect(item).to.have.property('totalAllocation');
    	});
    });

    it("Calculate total budget given a hierarchy of roles, with a combination of Department and root level Managers", function() {
    	var hierarchy = JSON.parse( fs.readFileSync('test/data_7200.json', 'utf-8') );

    	var budget = allocator.calculateBudget(hierarchy);

    	expect(budget.total).to.equal(7200);
    });

    it("Calculate total budget given a hierarchy of roles, with a combination of Department and root level Managers", function() {
    	var hierarchy = JSON.parse( fs.readFileSync('test/data_5400.json', 'utf-8') );

    	var budget = allocator.calculateBudget(hierarchy);

    	expect(budget.total).to.equal(5400);
    });

    it("Create enriched hierarchy and perform a lookup of budget data", function() {
        var hierarchy = JSON.parse( fs.readFileSync('test/data_5400.json', 'utf-8') );

        var budget = allocator.calculateBudget(hierarchy);
        
        // Uncomment if you'd like to output a sample of the enriched data 
        //fs.writeFile('test/output.json', JSON.stringify(budget));
        
        var bobsMoney = allocator.lookupTotalForName("Bob", budget.budgetData);

        expect(bobsMoney).to.equal(1800);
    });

  });
});