// Here is our assumed configuration.
var allocationConfig = [
	{ "role": "Department", "allocation": 0 },
	{ "role": "Manager", "allocation": 300 },
	{ "role": "QA Tester", "allocation": 500 },
	{ "role": "Developer", "allocation": 1000}
];
exports.allocationConfig = allocationConfig;

// Expects a simple hierarchy of Manager, QA Tester and Developer objects or strings
var calculateBudget = function(roleHierarchy) {
	var budgetTotal = 0;

	var itsAllGood = validateInput(roleHierarchy);
	if(!itsAllGood) return "ERROR IN INPUT";

	var hierarchyWithAllocationMap = createMapWithAllocations(roleHierarchy);

	// here we perform a reduction on each root element, as the overall JSON may represent
	// one or many departments, or managers
	hierarchyWithAllocationMap.forEach(function(item) {
		budgetTotal += calculateTotalReduction(0, item);
	});

	return {
		"total": budgetTotal,
		"budgetData": hierarchyWithAllocationMap
	};
}
exports.calculateBudget = calculateBudget;

var findNodeByName = function(entityName, nodes) {
	var foundNode;

	nodes.forEach(function(item) {
		if(item.name == entityName) {
			foundNode = item;
			return;
		} else {
			if(foundNode) return;
			foundNode = findNodeByName(entityName, item.reports);
		}
	});

	return foundNode;
}
exports.findNodeByName = findNodeByName;

/*
	Finds a node and returns its totalAllocation value
*/
var lookupTotalForName = function(entityName, budgetData) {
	var droidWeAreLookingFor = findNodeByName(entityName, budgetData);
	return droidWeAreLookingFor.totalAllocation;
}
exports.lookupTotalForName = lookupTotalForName;

/*
	Reduces a role node into a total (including the allocations for its children)
	While performing the calculation, the method also updates each object's 
	totalAllocation property.
*/
var calculateTotalReduction = function(amount, roleNodeItem) {
	var rollUp = roleNodeItem.allocation + 
			roleNodeItem.reports.reduce(calculateTotalReduction, amount);

	if(roleNodeItem.reports.length === 0) {
		roleNodeItem.totalAllocation = roleNodeItem.allocation;
	} else {
		roleNodeItem.totalAllocation = rollUp;
	}

	return rollUp;
}
exports.calculateTotalReduction = calculateTotalReduction;

/*
	This method creates a new map of roles, just like the original but with the added property
	'allocation' next to each role.
*/
var createMapWithAllocations = function(roleHierarchy) {
	return newRoleMap = roleHierarchy.map(enrichedRole = function(item) {
		return { 
			'role': item.role,
			'name': item.name,
			'reports': item.reports ? createMapWithAllocations(item.reports): [],
			'allocation': lookupBudgetForRole(item.role),
			'totalAllocation': 0
		};
	});
}
exports.createMapWithAllocations = createMapWithAllocations;

/*
	Returns the budget amount for a given role name
*/
var lookupBudgetForRole = function(roleName) {
	var item = allocationConfig.filter(function(roleItem) {
		return roleItem.role === roleName;
	});

	if(item.length >= 1) return item[0].allocation;

	return 0;
}
exports.lookupBudgetForRole = lookupBudgetForRole;

var validateInput = function(roleHierarchy) {
	var isValid = true;
	var errors = [];

	roleHierarchy.forEach(function(item) {
		if(!("role" in item) || !("name" in item)) {
			isValid = false;
			errors.push({
					"item": item,
					"error": "Encountered an item with a missing role or name property."
				});
		} else if(item.role.length === 0 || item.name.length === 0) {
			isValid = false;
			errors.push({
					"item": item,
					"error": "Encountered an item with a blank role or name."
				});

		} else if(item.role === "QA Tester" || item.role === "Developer") {
			if(item.reports && item.reports.length > 0) {
				isValid = false;
				errors.push({
					"name": item.name,
					"role": item.role,
					"error": item.role + ':' + item.name + ' has ' + 
						item.reports.length + ' direct reports (employees), ' + 
						'and should not have any.'
				});
			}
		}

		if("reports" in item) {
			isValid = validateInput(item.reports);
		}
		
	});

	if(!isValid) {
		console.log("ERRORS:");
		console.log(errors);
	}

	return isValid;
}
exports.validateInput = validateInput;